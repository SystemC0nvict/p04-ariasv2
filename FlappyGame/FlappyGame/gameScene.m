//
//  gameScene.m
//  FlappyGame
//
//  Created by Neo SX on 3/13/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import "gameScene.h"
#import "scrollBack.h"

static const uint32_t megaman_cat = 0x1 << 1;
static const uint32_t block_cat = 0x1 << 0;

#define BACK_SCROLLING_SPEED .5
@interface gameScene()

@property BOOL contentCreated;

@end

@implementation gameScene{
    scrollBack * back;
    SKSpriteNode *megaman;
    SKSpriteNode *floor;
    //SKSpriteNode *floors[10];
    CGPoint mvel;
    CGPoint bvels[10];
    SKTexture *walk1;
    SKTexture *walk2;
    SKAction *walkr;
    int p;
    int e;
    
}



- (void)didMoveToView:(SKView *)view{
    if(!self.contentCreated)
    {
        //[self makeBack];
        p = 0;
        e = 10;
        walk1 = [SKTexture textureWithImageNamed:@"walk1"];
        walk2 = [SKTexture textureWithImageNamed:@"walk2"];
        walkr = [SKAction repeatActionForever:[SKAction animateWithTextures:@[walk1,walk2] timePerFrame:.5 resize:YES restore:NO]];
        self.physicsWorld.contactDelegate = self;
        [self createSceneContents];
        self.contentCreated = YES;
        
    }
}

-(void)createSceneContents
{
   // self.backgroundColor = [SKColor colorWithPatternImage: [UIImage imageNamed:@"mx1intro"]];
    [self makeBack];
    self.scaleMode = SKSceneScaleModeAspectFit;
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
   [self addChild:[self megamanobj]];
    //for(int i = 0; i < e; i++){
        [self addChild:[self floormake]];
    //}
}

-(SKSpriteNode *)megamanobj
{
    mvel.x = 0;
    mvel.y = -20;
    megaman = [SKSpriteNode spriteNodeWithImageNamed:@"spawn"];
    megaman.position = CGPointMake(70, 650);
    megaman.physicsBody = [SKPhysicsBody bodyWithTexture:megaman.texture size:megaman.size];
  //  megaman.physicsBody.velocity =
    megaman.physicsBody.affectedByGravity = YES;
    megaman.physicsBody.dynamic = YES;
    //[self addChild:megaman];
    megaman.physicsBody.categoryBitMask = megaman_cat;
    megaman.physicsBody.contactTestBitMask = block_cat;
    return megaman;
}

-(SKSpriteNode *)floormake
{
   // if(bvels[p-1].x > self.frame.size.width)
    floor = [SKSpriteNode spriteNodeWithImageNamed:@"block"];
    floor.physicsBody = [SKPhysicsBody bodyWithTexture:floor.texture size:floor.size];
  //  floor.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:floor.texture.size];
    int randy = arc4random_uniform(300)+50;
    int randx = arc4random_uniform(bvels[p-1].x) + 400;
    if(p == 0){
        randy = 50;
        randx = 100;
    }
    floor.position = CGPointMake(randx, randy);
    floor.physicsBody.categoryBitMask = block_cat;
    floor.physicsBody.contactTestBitMask = megaman_cat;
    floor.physicsBody.affectedByGravity = FALSE;
    floor.physicsBody.dynamic = NO;
   // floors[p] = floor;
    
    CGPoint b = floor.position;
    bvels [p] = b;
    p++;
   /* if(bvels[p-1].x+400 > self.frame.size.width){
        p -= 1;
        e = 10;
    }*/
    return floor;
}

-(void)makeBack{
    back = [scrollBack scrollingNodeWithImageNamed:@"mx1intro" inContainerWidth:self.size.width];
    //[back setScrollSpeed:BACK_SCROLLING_SPEED];
   // [back setAnchorPoint:CGPointZero];
    [self addChild:back];
}

-(void)update:(NSTimeInterval)currentTime
{
    CGPoint mps = megaman.position;
    mps.x += mvel.x;
    mps.y += mvel.y;
    megaman.position = mps;
    
    /*for(int x = 0; x < p; x++){
        CGPoint bps = floors[x].position;
        bps.x = bvels[x].x;
    }*/
    if(mvel.y < 0 && mvel.x == 0){
        //for(int i; i < p; i++){
            CGRect f = [floor frame];
           // CGRect f = [floors[i] frame];
          //  NSData *imageData = [NSData dataWithContentsOfFile:@"block"];
          //  UIImage *image = [UIImage imageWithData:imageData];
         //   CGRect f = [initWithFrame: CGRectMake(100, 50, image.size.width, image.size.height)];
            CGRect m = [megaman frame];
         //[megaman setTexture:[SKTexture textureWithImageNamed:@"stand"]];
                if(CGRectIntersectsRect(m, f)){
                    mvel.y = 0;
                    //mvel.x = 5;
                    /*for(int v = 0; v < p; v++){
                        bvels[v].x = -2;
                    }*/
               // [back setScrollSpeed:BACK_SCROLLING_SPEED];

                //[megaman setTexture:[SKTexture textureWithImageNamed:@"stand"]];
                //[megaman runAction:[SKAction animateWithTextures:@[walk1,walk2] timePerFrame:.5 resize:YES restore:NO]];
                //[megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"stand"] resize:YES]];
                }
       // }
    }
    if(mvel.y == 0 && mvel.x == 0){
        //if(megaman.texture == walk2){
            //[megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"walk1"] resize:YES]];
        //}
        [megaman runAction:walkr];
        //[SKAction waitForDuration:.5];
       // [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"walk2"] resize:YES]];
       // [megaman runAction:[SKAction animateWithTextures:@[walk1,walk2] timePerFrame:.5 resize:YES restore:NO]];
        mvel.x = 5;
    }
    if(mvel.y > 0 && mvel.x > 0){
        [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"jump"] resize:YES]];
    }
    if(mvel.y < 0 && mvel.x >0){
        [megaman runAction:[SKAction setTexture:[SKTexture textureWithImageNamed:@"fall"] resize:YES]];
    }
    //if()
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    //mvel.y = 10;
    if(mvel.y == 0){
        //[megaman.physicsBody applyImpulse:CGVectorMake(5,15)];
        mvel.y = 10;
        //[SKAction waitForDuration:3];
        //mvel.y  = 0;
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    if(mvel.y > 0){
        mvel.y = -5;
    }
}

-(void)didBeginContact:(SKPhysicsContact *)contact{
    SKPhysicsBody *body1 , *body2;
    if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        body1 = contact.bodyA;
        body2 = contact.bodyB;
    }
    else{
        body1 = contact.bodyB;
        body2 = contact.bodyA;
    }
    if((body1.categoryBitMask == megaman_cat)&&(body2.categoryBitMask == block_cat)){
        [megaman runAction:[SKAction animateWithTextures:@[walk1,walk2] timePerFrame:.5 resize:YES restore:NO]];
    }
}

@end
