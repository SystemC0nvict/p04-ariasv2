//
//  scrollBack.m
//  FlappyGame
//
//  Created by Neo SX on 3/15/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import "scrollBack.h"

@implementation scrollBack

+(id) scrollingNodeWithImageNamed:(NSString *)mx1intro inContainerWidth:(float)width
{
    UIImage * image = [UIImage imageNamed:mx1intro];
    scrollBack * rnode = [scrollBack spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(width, image.size.height)];
    rnode.scrollSpeed = 1;
    
    float total = 0;
    while(total<(width+image.size.width)){
        SKSpriteNode * child = [SKSpriteNode spriteNodeWithImageNamed:mx1intro];
        [child setAnchorPoint:CGPointZero];
        [child setPosition:CGPointMake(total, 0)];
        [rnode addChild:child];
        total+=child.size.width;
    }
    return rnode;
}

-(void) update:(NSTimeInterval)currentTime
{
    [self.children enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop){
        SKSpriteNode *child = (SKSpriteNode *) object;
        child.position = CGPointMake(child.position.x-self.scrollSpeed, child.position.y);
        if(child.position.x <= -child.size.width){
            float delta = child.position.x+child.size.width;
            child.position = CGPointMake(child.size.width*(self.children.count-1)+delta, child.position.y);
        }
    }];
}

@end
